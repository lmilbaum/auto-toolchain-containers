# Shell testing

A container image for tools used at different jobs at the CI pipelines for
linting and testing shellscripts.

The container is based on **tools-container** and contains the following extra tools:

- shellcheck (linting)
- shellspec (testing framework)
- kcov (code coverage tool)

