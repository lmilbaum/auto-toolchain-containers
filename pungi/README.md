# Pungi

A container image for tools used at different jobs at the CI pipelines for
creating composes

The container is based on fedora:38 and contains the following extra tools:

- Development Tools
- rpm-build
- rpmdevtools
- createrepo_c
- isomd5sum
- genisoimage
- syslinux
- cmake
- ninja-build
- bzip2
- curl
- libxml
- openssl
- glib2
- sqlite
- zchunk
- modulemd
- libicu
- krb5
